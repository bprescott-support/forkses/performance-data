require 'tty-spinner'

module DataDuplicator
  extend self

  def duplicate(entities:, entity_count:, entity_name:)
    return false if entities.nil?

    perform_with_spinner("Duplicating #{entity_name} from the existing #{entities.count}") do
      entities = duplicate_entities(entities, entity_count)
      entities.each.with_index do |entity, index|
        entity["iid"] = index + 1
      end
    end
    entities
  end

  def perform_with_spinner(action)
    spinner = TTY::Spinner.new("[:spinner] #{action}")
    spinner.auto_spin
    yield
    spinner.success
  end

  def deep_copy(object)
    Marshal.load(Marshal.dump(object))
  end

  def duplicate_entities(array, max_count)
    result_amount = array.count + max_count
    array += deep_copy(array) until array.count > max_count
    array.first(result_amount)
  end
end
