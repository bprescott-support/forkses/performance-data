# Project Data Generator Tool

[[_TOC_]]

The Data Generator Tool can seed the following data to the specified GitLab project via [API](https://docs.gitlab.com/ee/api/):
- Merge Requests with comments and labels
- Issues with comments and labels

The full options for running the tool can be seen by getting the help output by running `bin/generate-project-data --help`:

```
Generate Data for GitLab Project.
Usage: generate-project-data [options]

Options:
  --environment-url=<s>              The environment's full URL
  --project-id=<s>                   Project ID to generate the data against
  -m, --merge-requests=<i>           Number of merge requests to create
  -i, --issues=<i>                   Number of issues to create
  -l, --labels=<i>                   Number of labels to create
  -c, --comments=<i>                 Number of comments to create for every merge request and / or issue (default: 20)
  -o, --closed-merge-requests=<i>    Percentage of merge requests to create as closed
  -s, --closed-issues=<i>            Percentage of issues to create as closed
  -f, --force                        Skip the data injection warning
  -h, --help                         Show help message

Environment Variables:
  ACCESS_TOKEN             A valid GitLab Personal Access Token for the specified environment. The token should come from a User that has admin access for the
project and have API permission. (Default: nil)

Examples:
  Generate 100 merge requests with default 20 comments in each:
  ./bin/generate-project-data --environment-url <URL> --project-id 12345 --merge-requests 100
  Generate 200 merge requests and 100 issues with 50 labels and 30 comments in each:
  ./bin/generate-project-data --environment-url <URL> --project-id 12345 --merge-requests 200 --issues 100 --labels 50 --comments 30
  Generate 200 merge requests and 100 issues with 20% and 40% closed respectively:
  ./bin/generate-project-data --environment-url <URL> --project-id 12345 --merge-requests 200 --issues 100 --closed-merge-requests 20 --closed-issues 40
```

To get project ID navigate to the project home page and copy `Project ID` number.

Output example for generating 1000 merge requests, 3000 issues, and 500 labels:
```
Checking that GitLab environment 'http://10k.testbed.gitlab.net' is available and that provided Access Token works...
Environment and Access Token check was successful - URL: http://10k.testbed.gitlab.net, Version: 12.7.0-pre 214c605dd1b
Starting to generate data for project Linux...
Generating labels: 100% |====================================================================>| Time: 00:01:24
Generating merge requests: 100% |============================================================>| Time: 03:22:21
Generating comments for MRs: 100% |==========================================================>| Time: 00:41:05
Generating issues: 100% |====================================================================>| Time: 00:16:26
Generating comments for Issues: 100% |=======================================================>| Time: 01:45:15

```

## Generate Merge Requests

To generate Merge Requests execute: `./bin/generate-project-data --environment-url <URL> --project-id 12345 --merge-request X`, where X - number of merge requests to create.

The tool will do the following steps:
1. Fetch existing labels and commits.
2. Create a new branch
3. Generate commits for the new branch: 
  - Generate random amount of [commits](https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions) between `MIN_COMMITS_COUNT_IN_MR`(default: 1) and `MAX_COMMITS_COUNT_IN_MR`(default: 5)
    - Each generated commit will have one "update" action and random amount of "create" actions between `MIN_ACTIONS_COUNT_IN_COMMIT`(default: 1) and `MAX_ACTIONS_COUNT_IN_COMMIT`(default: 5). 
4. Create merge request from the new branch with random `MAX_LABELS_COUNT`(default: 3) labels.
5. After all merge requests are generated, the tool will generate a random amount of comments for merge request between `MIN_COMMENTS_COUNT`(default: 1) and `--comments`(default: 20) with a link to a random merge request.

Other command examples:
- Create 10 merge requests with 20% closed and with 500 commits in each one:
  - `MIN_COMMITS_COUNT_IN_MR=500 MAX_COMMITS_COUNT_IN_MR=500 ./bin/generate-project-data --environment-url <URL> --project-id 12345 --merge-request 10 --closed-merge-requests 20`
- Create 1 merge request with 10 commits and 50 actions in each one:
  - `MIN_COMMITS_COUNT_IN_MR=10 MAX_COMMITS_COUNT_IN_MR=10 MIN_ACTIONS_COUNT_IN_COMMIT=50 MAX_ACTIONS_COUNT_IN_COMMIT=50 ./bin/generate-project-data --environment-url <URL> --project-id 12345 --merge-request 1`  

## Generate Issues

To generate Issues execute: `./bin/generate-project-data --environment-url <URL> --project-id 12345 --issues X`, where X - number of issues to create.
The tool will do the following steps:
1. Fetch 100 existing merge requests or use merge requests that were generated in the previous step. These merge requests will be used in the comments to create more relations between Issues and Merge Requests
1. Create issue with random `MAX_LABELS_COUNT`(default: 3) labels.
1. After all issues are generated, the tool will generate a random amount of comments for issues between `MIN_COMMENTS_COUNT`(default: 1) and `comments`(default: 20) with a link to a random merge request and issue.

## Generate Labels

To generate Labels execute: `./bin/generate-project-data --environment-url <URL>  --project-id 12345 --labels X`, where X - number of labels to create. If the number is not specified, script will collect all existing project labels to use them for merge requests and/or issues.

